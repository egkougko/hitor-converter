# HitOr Converter

This project contains the schematics and layout of an adjustable level shifter board, transforming CMOS level input signals (< 2 V>) to TTL standard square pulses.
The interface was designed for the FEi4 HitOr bus in mind but can be adjusted to other ASICs. The board uses a single 12V input with though a standard barrel jack connector.
Models for a 3D printed enclosure are also provided as well as a parts list, geber files and reference to a DESY technical note.
